/*

1. In the S27 folder, create an a1 folder and an index.js file inside of it.
2. Create a simple server and the following routes with their corresponding HTTP methods and responses:
a. If the url is http://localhost:4000/, send a response Welcome to Booking System
b. If the url is http://localhost:4000/profile, send a response Welcome to your profile!
c. If the url is http://localhost:4000/courses, send a response Here’s our courses available
d. If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
e. If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
f. If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
3. Test all the endpoints in Postman.
4. Create a git repository named S27.
5. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
6. Add the link in Boodle.

*/

let http = require("http");
let port = 4000;

http.createServer((request, response) =>{

	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write("Welcome to your profile!");
		response.end();
	}

	else if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write("Here’s our courses available");
		response.end();
	}
	else if(request.url == "/addcourse" && request.method == "POST"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write("Add a course to our resources");
		response.end();
	}
	else if(request.url == "/updatecourse" && request.method == "PUT"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write("Update a course to our resources");
		response.end();
	}
	else if(request.url == "/archivecourses" && request.method == "DELETE"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write("Archive courses to our resources");
		response.end();
	}
	else if(request.url == "/")
	{
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write("Welcome to Booking System");
		response.end();
	}

}).listen(port);

console.log(`Server is running at localhost:${port}`);
